﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace LabelPrintingVestey.Models
{
    public class LabelModel
    {
        [Required(ErrorMessage = "*Sales order no. is required")]
        public string SalesOrderNo { get; set; }

        [Required(ErrorMessage = "*No. of boxes is required")]
        public int NoOfBoxes { get; set; }
        public string Printer { get; set; }
        public string DeliveryDate { get; set; }
    }


    public class OrderDetails
    {
        public string OrderNo { get; set; }
        public string SupplierReferenceNo { get; set; }
        public string CustomerReferenceNo { get; set; }
        public string CustomerAccountName { get; set; }
        public string Address { get; set; }
        public string RouteNo { get; set; }
        public string DropNo { get; set; }
        public string DeliveryDate { get; set; }
        public string Box { get; set; }

    }


    //According to procedure return fields
    public class AllOrders
    {
        public Int64 ID { get; set; }
        public string SourceOrderID { get; set; }
        public string ConsolidatedPONumber { get; set; }
        public string CustomerAccountCode { get; set; }
        public string CustomerAccountName { get; set; }
        public string CustomerAccountLocation { get; set; }
        public string AdditionalInfo1 { get; set; }
        public string AdditionalInfo2 { get; set; }
        public string AdditionalInfo3 { get; set; }
        public string OracleOrderNo { get; set; }
        public string CustomerDeliveryDate { get; set; }
        public string OurRoute { get; set; }
        public string PaymentTerm { get; set; }
        public string OracleOrderNumber { get; set; }
        public string Depot { get; set; }
        public string OrderNo { get; set; }
        public string CustomerSite { get; set; }
        public string OurSite { get; set; }



    }

}