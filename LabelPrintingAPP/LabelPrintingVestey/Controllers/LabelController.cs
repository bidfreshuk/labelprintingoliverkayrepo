﻿
using System.Collections.Generic;
using System.Web.Mvc;
using LabelPrintingVestey.Models;
using LabelPrintingVestey.Baseclass;
using System.Data;
using System.Drawing.Printing;
using System.Management;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System;
using Newtonsoft.Json;

namespace LabelPrintingVestey.Controllers
{
    public class LabelController : Controller
    {
        // GET: Label
        public ActionResult Index()
        {
            
            return View();
        }

        //public ActionResult PrintLabel(LabelModel mdl)
        //{

        //    if (ModelState.IsValid) {
        //        DB _db = new DB();
        //        var ds = _db.GetOrderDetails(mdl.SalesOrderNo);
        //        PrintLanscape(ds,mdl);
        //    }
        //        SelectList lst = new SelectList(new List<SelectListItem> {
        //    new SelectListItem { Text ="Printronix Auto ID T8204 - PGL",Value="Printronix Auto ID T8204 - PGL" },
        //     }, "Value", "Text");

        //        ViewBag.Printer = lst;
            

        //    return View("Index");
        //}

        //private void GetAllPrinterList()
        //{
        //    ManagementScope objScope = new ManagementScope(ManagementPath.DefaultPath); //For the local Access
        //    objScope.Connect();
        //    List<string> lstPrinterList = new List<string>();
        //    SelectQuery selectQuery = new SelectQuery();
        //    selectQuery.QueryString = "Select * from win32_Printer";
        //    ManagementObjectSearcher MOS = new ManagementObjectSearcher(objScope, selectQuery);
        //    ManagementObjectCollection MOC = MOS.Get();
        //    foreach (ManagementObject mo in MOC)
        //    {
        //        lstPrinterList.Add(mo["Name"].ToString());
        //    }
        //}

        public void Print()
        {
           
            //from file
            //-------------
            //var document2 = new HtmlAgilityPack.HtmlDocument();
            //document2.Load(@"C:\Temp\Template.html");                             //(@"C:\Sourcecode\LabelPrintingVestey\LabelPrintingVestey\File\Template.html");
            //var nodes = document2.DocumentNode.SelectSingleNode("//table").InnerText;
            //-------------


            PrintDocument doc = new PrintDocument();
            doc.OriginAtMargins = false;

            doc.PrinterSettings.PrinterName = "Printronix Auto ID T8204 - PGL"; // System.Web.HttpUtility.UrlDecode("\\\\BFLHLWSVR0002.shl.lan\\Bidfresh Suite Henson MFP");  
            doc.PrintPage += (sender, ppeArgs) =>
            {
                int x = 10, y = 10;
                int width = 150, height = 40;

                System.Drawing.Font FontNormal = new System.Drawing.Font("Arial",12);
                Graphics g = ppeArgs.Graphics;
                Image i = Image.FromFile("C://Sourcecode//LabelPrintingVestey//LabelPrintingVestey//Images//OliverKayLogo1017.png");
                g.DrawImage(i, 185, 10, width, height);

                x += 0;
                y += height + 30;

                int hdy = (int)FontNormal.GetHeight(g) + 10; ;

                g.DrawString("Order No : 19717858", FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;
                g.DrawString("Delivery Date: 30/09/2019", FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;

                float width0 = 330.0F;
                float height0 = 30.0F;
                RectangleF drawRect0 = new RectangleF(x, y, width0, height0);
                int hdy0 = (int)drawRect0.Height + 10;
                // Draw rectangle to screen.
                Pen blackPen0 = new Pen(Color.Black);
                g.DrawRectangle(blackPen0, x, y, width0, height0);

                // Set format of string.
                StringFormat drawFormat0 = new StringFormat();
                drawFormat0.Alignment = StringAlignment.Near;

                g.DrawString("Route No.: 531       Drop No.: 92500", FontNormal, Brushes.Black, drawRect0, drawFormat0); y += hdy0;


                g.DrawString("Customer Reference No.: 71101752", FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;
                g.DrawString("Customer Account Name:  T.JS DINER", FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;
                // Create rectangle for drawing.
               
                float width1 = 330.0F;
                float height1 = 60.0F;
                RectangleF drawRect = new RectangleF(x, y, width1, height1);
                hdy = (int)drawRect.Height + 10;
                // Draw rectangle to screen.
                Pen blackPen = new Pen(Color.Black);
                g.DrawRectangle(blackPen, x, y, width1, height1);

                // Set format of string.
                StringFormat drawFormat = new StringFormat();
                drawFormat.Alignment = StringAlignment.Near;
                //e.Graphics.DrawString(drawString, drawFont, drawBrush, drawRect, drawFormat);
                g.DrawString("Address: HENDERSON RESTURANT GROUP LTD, 6 MAIN STREET TYNDRUM , FK20 8RY", FontNormal, Brushes.Black, drawRect, drawFormat); y += hdy;
                g.DrawString("Box: 1/1", FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;
              

                //g.DrawString(nodes, FontNormal, Brushes.Black, new PointF(x, y)); y += hdy; // 0, 0, new StringFormat());
            };

            Margins margins = new Margins(10, 10, 10, 10);
            doc.DefaultPageSettings.Margins = margins;
            //doc.DefaultPageSettings.Landscape = true;
            
            //PrintDialog pdi = new PrintDialog();
            //pdi.Document = doc;
            PrintPreviewDialog printPrvDlg = new PrintPreviewDialog();
            printPrvDlg.Document = doc;
            printPrvDlg.ShowDialog();

           // if (pdi.ShowDialog() == DialogResult.OK)
           // {
                doc.Print();
           // }
        }

        public void PrintLanscape(DataSet ds, LabelModel mdl)
        {
            //from file
            //-------------
            //var document2 = new HtmlAgilityPack.HtmlDocument();
            //document2.Load(@"C:\Temp\Template.html");                             //(@"C:\Sourcecode\LabelPrintingVestey\LabelPrintingVestey\File\Template.html");
            //var nodes = document2.DocumentNode.SelectSingleNode("//table").InnerText;  mdl.Printer;
            //-------------
            if (ds.Tables[0].Rows.Count > 0) {
                for (int j = 1; j <= mdl.NoOfBoxes; j++) {

                    PrintDocument doc = new PrintDocument();
                    doc.OriginAtMargins = false;
                    doc.PrinterSettings.PrinterName = System.Web.HttpUtility.UrlDecode(mdl.Printer);  //System.Web.HttpUtility.UrlDecode("\\\\BFLHLWSVR0005\\SHARP_VEST");  //PrinterName;
                    doc.PrintPage += (sender, ppeArgs) =>
                    {
                        int x = 10, y = 10;
                        int width = 150, height = 40;

                        System.Drawing.Font FontNormal = new System.Drawing.Font("Arial", 12);
                        Graphics g = ppeArgs.Graphics;
                        Image i = Image.FromFile("C://Sourcecode//LabelPrintingVestey//LabelPrintingVestey//Images//OliverKayLogo1017.png");
                        //Image i = Image.FromFile("C://inetpub//wwwroot//OKPLabelPrint//Images//OliverKayLogo1017.png");
                        g.DrawImage(i, 285, 10, width, height);

                        x += 0;
                        y += height + 20;
                        int hdy = (int)FontNormal.GetHeight(g) + 10;
                       

                        //-------------------start writing string--------------------------------------//
                        System.Drawing.Font FontHeader = new System.Drawing.Font("Arial", 18);
                        int hdy00 = (int)FontHeader.GetHeight(g) + 10;
                        g.DrawString(ds.Tables[0].Rows[0]["CustomerAccountCode"] + " (" + ds.Tables[0].Rows[0]["OrderNo"] + ")           " + Convert.ToDateTime(ds.Tables[0].Rows[0]["CustomerDeliveryDate"]).ToShortDateString(), FontHeader, Brushes.Black, new PointF(x, y)); y += hdy00;
                        //g.DrawString("Delivery Date: " + ds.Tables[0].Rows[0]["CustomerDeliveryDate"], FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;

                        float width0 = 430.0F;
                        float height0 = 30.0F;
                        RectangleF drawRect0 = new RectangleF(x, y, width0, height0);
                        int hdy0 = (int)drawRect0.Height + 10;
                        //Draw rectangle to screen.
                        Pen blackPen0 = new Pen(Color.Black);
                        g.DrawRectangle(blackPen0, x, y, width0, height0);

                        // Set format of string.
                        StringFormat drawFormat0 = new StringFormat();
                        drawFormat0.Alignment = StringAlignment.Near;

                        g.DrawString("Route No.: " + ds.Tables[0].Rows[0]["AdditionalInfo3"] , FontNormal, Brushes.Black, drawRect0, drawFormat0); y += hdy0;


                        g.DrawString("Reference No.: " +  ds.Tables[0].Rows[0]["OracleOrderNumber"] , FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;
                        System.Drawing.Font FontNormal1 = new System.Drawing.Font("Arial", 12,FontStyle.Bold);
                        //FontNormal1.Bold(true);
                        int y12 = x + 100;
                        g.DrawString("Customer Account Name: " , FontNormal, Brushes.Black, new PointF(x, y));   //y += hdy;
                        g.DrawString("                                         " + ds.Tables[0].Rows[0]["CustomerAccountName"], FontNormal1, Brushes.Black, new PointF(x, y)); y += hdy;
                        // Create rectangle for drawing.

                        float width1 = 430.0F;
                        float height1 = 55.0F;
                        RectangleF drawRect = new RectangleF(x, y, width1, height1);
                        hdy = (int)drawRect.Height + 10;

                    // Draw rectangle to screen.
                        Pen blackPen = new Pen(Color.Black);
                        g.DrawRectangle(blackPen, x, y, width1, height1);

                    // Set format of string.
                        StringFormat drawFormat = new StringFormat();
                        drawFormat.Alignment = StringAlignment.Near;

                        //g.DrawString("Address: " + ds.Tables[0].Rows[0]["CustomerSite"] + "," + ds.Tables[0].Rows[0]["Address2"] + ds.Tables[0].Rows[0]["Address3"] + ds.Tables[0].Rows[0]["AddressPost"] + ds.Tables[0].Rows[0]["Postcode"], FontNormal, Brushes.Black, drawRect, drawFormat); y += hdy;
                        System.Drawing.Font FontNormal12 = new System.Drawing.Font("Arial", 18);
                        g.DrawString("Address: " + ds.Tables[0].Rows[0]["CustomerSite"], FontNormal12, Brushes.Black, drawRect, drawFormat); y += hdy;
                        g.DrawString("Box: " + j+"/"+mdl.NoOfBoxes, FontNormal, Brushes.Black, new PointF(x, y)); y += hdy;


                    //g.DrawString(nodes, FontNormal, Brushes.Black, new PointF(x, y)); y += hdy; // 0, 0, new StringFormat());
                };

                    Margins margins = new Margins(30, 10, 10, 10);
                    doc.DefaultPageSettings.Margins = margins;
                    doc.DefaultPageSettings.Landscape = true;


                    PrintPreviewDialog printPrvDlg = new PrintPreviewDialog();
                    printPrvDlg.Document = doc;
                    printPrvDlg.ShowDialog();
                    try
                    {
                       // doc.Print();
                    }catch(Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }


        /* Autocomplete text box code */
        [HttpPost]
        public JsonResult GetListOracleOrderNo(string Prefix)
        {
            //Note : you can bind same list from database  
            List<string> ObjList = new List<string>();
            DB _db = new DB();
            ObjList = _db.GetListOracleOrderNo(Prefix);

            //Searching records from list using LINQ query  
            var CityList = (from N in ObjList
                            where N.StartsWith(Prefix)
                            select new { N });
            return Json(ObjList, JsonRequestBehavior.AllowGet);
        }


        /*GetAllOrdes for grid*/
        public JsonResult GetAllOrders(string searchbyDeliveryDate)
        {
            DataSet ds = null; 
            DB _db = new DB();
            ds = _db.GetAllOrders(searchbyDeliveryDate);
            List<AllOrders> allOrders = new List<AllOrders>();
            allOrders = _db.ConvertToList<AllOrders>(ds.Tables[0]);
            return new JsonResult { Data = allOrders, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /*Print label*/
        [HttpPost]
        public JsonResult PrintLabel(string orderno, int noofbox, string printer)
        {
            try
            {
                LabelModel mdl = new LabelModel();
                mdl.SalesOrderNo = orderno;
                mdl.NoOfBoxes = noofbox;
                mdl.Printer = printer;
                DB _db = new DB();
                var ds = _db.GetOrderDetails(mdl.SalesOrderNo);
                PrintLanscape(ds, mdl);
                return Json("Success", JsonRequestBehavior.AllowGet);
                //Searching records from list using LINQ query  
            }
            catch(Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
          
           
        }
    }
}