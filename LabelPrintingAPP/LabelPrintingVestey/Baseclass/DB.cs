﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Globalization;


namespace LabelPrintingVestey.Baseclass
{
    public class DB
    {
        static private string procedureName = "[VESTEY].[GetListForSearchingOrder]";
        static private string allorderprocedureName = "[VESTEY].[OrderDetailsForReporting]";
        public DataSet GetOrderDetails(string orderNo)
        {
            String[] strlist = orderNo.Split('/');
            var vesteyOrdNo = strlist[0];
            var oracleOrdNo = strlist[1];
            var _orderNo = DBNull.Value;
            //if (oracleOrdNo == null || oracleOrdNo == "")
            //{
            //    vesteyOrdNo = strlist[0];
            //}
            //else
            //{
            //    vesteyOrdNo = DBNull.Value;
            //}
             DataSet ds = new DataSet();
            List<string> objList = new List<string>();
            string ConnectionString = ConfigurationManager.ConnectionStrings["IT"].ConnectionString;
            SqlConnection cnn = new SqlConnection(ConnectionString);

            cnn.Open();
            SqlCommand sqlComm = new SqlCommand(allorderprocedureName, cnn);
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.Parameters.Add(new SqlParameter("@fileid", DBNull.Value));
            if (oracleOrdNo == "null")
            {
                sqlComm.Parameters.Add(new SqlParameter("@VesteyOrderNo", vesteyOrdNo));
            }
            else
            {
                sqlComm.Parameters.Add(new SqlParameter("@VesteyOrderNo", DBNull.Value));
            }

            if (oracleOrdNo != "null")
            {
                sqlComm.Parameters.Add(new SqlParameter("@OracleOrderNo ", oracleOrdNo));
            }
            else
            {
                sqlComm.Parameters.Add(new SqlParameter("@OracleOrderNo ", DBNull.Value));
            }
            sqlComm.Parameters.Add(new SqlParameter("@DelDate", DBNull.Value));
            SqlDataAdapter da = new SqlDataAdapter(sqlComm);
            da.Fill(ds);

            return ds;
        }

        public List<string> GetListOracleOrderNo(string prefix)
        {
            DataTable dt = new DataTable();
            List<string> objList = new List<string>();
            string ConnectionString = ConfigurationManager.ConnectionStrings["IT"].ConnectionString;
            SqlConnection cnn = new SqlConnection(ConnectionString);
            DataSet ds = new DataSet();
            cnn.Open();
            SqlCommand sqlComm = new SqlCommand(procedureName, cnn);
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.Parameters.Add(new SqlParameter("@Prefix", prefix));
            SqlDataAdapter da = new SqlDataAdapter(sqlComm);
            da.Fill(dt);

            foreach (DataRow dr in dt.Rows)
            {
                objList.Add(Convert.ToString(dr[0]));
            }
           

            cnn.Close();
            return objList;
        }

        public DataSet GetAllOrders(string datatxt)
        {


            DateTime _date = Convert.ToDateTime(datatxt);
            DataSet ds = new DataSet();
            List<string> objList = new List<string>();
            string ConnectionString = ConfigurationManager.ConnectionStrings["IT"].ConnectionString;
            SqlConnection cnn = new SqlConnection(ConnectionString);

            cnn.Open();
            SqlCommand sqlComm = new SqlCommand(allorderprocedureName, cnn);
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.Parameters.Add(new SqlParameter("@fileid", DBNull.Value));
            sqlComm.Parameters.Add(new SqlParameter("@VesteyOrderNo", DBNull.Value));
            sqlComm.Parameters.Add(new SqlParameter("@OracleOrderNo ", DBNull.Value));
            sqlComm.Parameters.Add(new SqlParameter("@DelDate", _date));
            SqlDataAdapter da = new SqlDataAdapter(sqlComm);
            da.Fill(ds);
            return ds;
        }

        //[HttpGet]
        //public List<GetAllOrders> GetAllOrders(string datatxt)
        //{
        //    SAPbobsCOM.Company oCompany = null;
        //    string docdue = "2019-10-26";
        //    //if (cardcode.IndexOf("/") > 0)
        //    //{
        //    //    cardcode = cardcode.Substring(0, cardcode.IndexOf("/"));
        //    //}

        //    ServerConnection connection = HTTPHelper.getConnection();
        //    if (!connection.Connected())
        //    {
        //        // LogHelper.Log("ERROR", connection.GetErrorMessage(), "New Order");

        //    }
        //    SAPbobsCOM.Documents oOrder;
        //    string strQuery = "";

        //    //var err = new Error();

        //    oCompany = connection.GetCompany();

        //    try
        //    {
        //        SAPbobsCOM.Recordset oRS;
        //        oRS = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //        //Get a list of open orders
        //        strQuery = "SELECT T0.DocEntry, T0.DocNum, T0.DocStatus, T0.DocDate, T0.CardCode, T0.NumAtCard, T0.DocTotal, T0.UpdateDate, T0.PickStatus, '' as PO_Number,T0.DocDueDate, T0.CardName, T0.Address, T0.VatSum, T0.DiscSum, T0.Confirmed, T0.CANCELED, '' as Status FROM ORDR T0 WHERE(T0.DocStatus = 'O' and T0.DocDueDate ='" + docdue + "')";
        //        oRS.DoQuery(strQuery);
        //        List<GetAllOrders> getOrders = new List<GetAllOrders>();
        //        if (oRS.RecordCount > 0)
        //        {
        //            do
        //            {
        //                GetAllOrders openOrder = new GetAllOrders();
        //                openOrder.DocEntry = oRS.Fields.Item("DocEntry").Value;
        //                openOrder.DocNum = oRS.Fields.Item("DocNum").Value;
        //                openOrder.DocStatus = oRS.Fields.Item("DocStatus").Value;
        //                openOrder.DocDate = oRS.Fields.Item("DocDate").Value;
        //                openOrder.AccountCode = oRS.Fields.Item("CardCode").Value;
        //                openOrder.Reference = oRS.Fields.Item("NumAtCard").Value;
        //                openOrder.DocTotal = oRS.Fields.Item("DocTotal").Value;
        //                openOrder.UpdateDate = oRS.Fields.Item("UpdateDate").Value;
        //                openOrder.PickStatus = oRS.Fields.Item("PickStatus").Value;
        //                openOrder.PO_Number = oRS.Fields.Item("PO_Number").Value;
        //                openOrder.DeliveryDate = oRS.Fields.Item("DocDueDate").Value;
        //                openOrder.AccountName = oRS.Fields.Item("CardName").Value;
        //                openOrder.Address = oRS.Fields.Item("Address").Value;
        //                openOrder.VatSum = oRS.Fields.Item("VatSum").Value;
        //                openOrder.DiscSum = oRS.Fields.Item("DiscSum").Value;
        //                openOrder.Confirmed = oRS.Fields.Item("Confirmed").Value;
        //                openOrder.Cancelled = oRS.Fields.Item("CANCELED").Value;
        //                openOrder.Status = oRS.Fields.Item("Status").Value;
        //                getOrders.Add(openOrder);
        //                oRS.MoveNext();
        //            }
        //            while (!oRS.EoF);
        //        }
        //        //no more processing, return info
        //        return getOrders;
        //    }
        //    catch (Exception ex)
        //    {

        //        //{
        //        //    err.ErrorCode = "505";
        //        //    err.ErrorRef = "API - Get customer orders";
        //        //    err.ErrorMsg = ex.Message;
        //        //};
        //    }
        //    finally
        //    {
        //        //oCompany.Disconnect();
        //    }

        //    //create empty list to return in case of error
        //    return new List<GetAllOrders>();

        //}

        /*Generic Method*/

        public List<T> ConvertToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>()
                    .Select(c => c.ColumnName)
                    .ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name))
                    {
                        if (pro.Name == "CustomerDeliveryDate")
                        {
                            string dateformat = Convert.ToDateTime(row[pro.Name]).ToShortDateString();
                            PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
                            pro.SetValue(objT, Convert.ChangeType(dateformat, pI.PropertyType));
                        }
                        else
                        {

                            PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
                            pro.SetValue(objT, row[pro.Name] == DBNull.Value ? null : Convert.ChangeType(row[pro.Name], pI.PropertyType));
                        }
                    }
                }
                return objT;
            }).ToList();
        }
    }
}