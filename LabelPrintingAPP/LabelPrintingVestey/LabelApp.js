﻿//(function () {
//Module 
var app = angular.module('LabelApp', ['ngRoute']);
// Controller
app.controller('AppLabelController', function ($scope, LabelService) {

    //$scope.Message = "Congratulation you have created your first application using AngularJs";
    $scope.Orders = null;
    $scope.searchDeliveryDate = null;
    $scope.changeformat = function () {
        var currentElement = $scope.searchbyDeliveryDate;
        var d = new Date(currentElement);
        var dd = d.getDate();
        var mm = d.getMonth();
        mm++;
        var yy = d.getFullYear();
        var newdate = yy + "-" + mm + "-" + dd;
        $scope.searchDeliveryDate = newdate;
    };


    $scope.printer =
        [
            { id: 0, type: "\\\\BFLHLWSVR0005\\LBL_WED", name: "Printronix Auto ID T8204 - PGL(Wednesbury)" },
            { id: 1, type: "\\\\BFLHLWSVR0005\\LBL_BOL", name: "Printronix Auto ID T8204 - PGL(Boltan)" },
            { id: 2, type: "\\\\BFLHLWSVR0005\\LBL_HEN", name: "Testing" }
        ];
    $scope.PrintModel =
        {
            OrderNo: '',
            NoOfBoxes: '',
            Printer: ''
        };


    $scope.GetRecordByDeliveryDate = function (isValid) {
        if (isValid) {
            $scope.message = '';
            LabelService.GetRecord($scope.searchDeliveryDate).then(function (d) {
                $scope.Orders = d.data; // Success
            }, function () {
                //alert('Failed'); // Failed
                $scope.message = 'No Records found !';
            });
        }
        $scope.message = 'Delivery date is required ';
    };

    $scope.PrintLabel = function (printModel) {
        var LabelModel = {};
        LabelModel["SalesOrderNo"] = printModel.OrderNo;
        LabelModel["NoOfBoxes"] = printModel.NoOfBoxes;
        LabelModel["Printer"] = printModel.Printer.type;
        LabelService.PrintLabel(LabelModel);
    };

    $scope.ShowPrintDialog = function (orderDetail) {
        //    var LabelModel = {};
        //    LabelModel["SalesOrderNo"] = PrinterModel.OrderNo;
        //    LabelModel["NoOfBoxes"] = PrinterModel.NoOfBoxes;
        //    LabelModel["Printer"] = PrinterModel.Printer.name;
        //    LabelService.PrintLabel(LabelModel);
        $(".divDialog").show();
        $scope.PrintModel.OrderNo = (orderDetail.OrderNo + "/" + orderDetail.OracleOrderNo);
    };

    $scope.hidePrintDailog = function () {
        $(".divDialog").hide();

    };
});


//The concept of the factory service is same as service layer in ASP.NET MVC Application.
app.factory('LabelService', function ($http) {
    var fac = {};
    //GetRecord function will call the GetStudentRecord action method.
    fac.GetRecord = function (_searchbyDeliveryDate) {
        return $http.get('/Label/GetAllOrders?searchbyDeliveryDate=' + _searchbyDeliveryDate);
    },

        fac.PrintLabel = function (labelModel) {

            return $http({
                url: '/Label/PrintLabel',
                method: 'POST',
                data: labelModel
            }).then(function (response) {
                //alert(response.d);
            }, function (error) {
                alert("error")
            });

        }
    //fac.PrintLabel = function (labelModel) {
    //  var def = $q.defer();
    //     $http({
    //        url: '/Label/PrintLabel',
    //        method: 'POST',
    //        data: labelModel
    //    }).then(function (response) {
    //        def.resolve(response);
    //        //alert(response.d);
    //    }, function (error) {
    //        //alert("error")
    //            def.reject("Failed to print");
    //    });
    //return def.promise;
    //alert("ser");
    // }
    return fac;



});

//app.config(
//    function simulateNetworkLatency($httpProvider) {
//        $httpProvider.interceptors.push(httpDelay);
//        // I add a delay to both successful and failed responses.
//        function httpDelay($timeout, $q) {
//            var delayInMilliseconds = 850;
//            // Return our interceptor configuration.
//            return ({
//                response: response,
//                responseError: responseError
//            });
//            // ---
//            // PUBLIC METHODS.
//            // ---
//            // I intercept successful responses.
//            function response(response) {
//                var deferred = $q.defer();
//                $timeout(
//                    function () {
//                        deferred.resolve(response);
//                    },
//                    delayInMilliseconds,
//                    // There's no need to trigger a $digest - the view-model has
//                    // not been changed.
//                    false
//                );
//                return (deferred.promise);
//            }
//            // I intercept error responses.
//            function responseError(response) {
//                var deferred = $q.defer();
//                $timeout(
//                    function () {
//                        deferred.reject(response);
//                    },
//                    delayInMilliseconds,
//                    // There's no need to trigger a $digest - the view-model has
//                    // not been changed.
//                    false
//                );
//                return (deferred.promise);
//            }
//        }
//    }
//);
//app.directive('datePicker', function () {
//    return {
//        restrict: 'A',
//        require: 'ngModel',
//        link: function (scope, elm, attr, ctrl) {

//            // Format date on load
//            ctrl.$formatters.unshift(function (value) {
//                if (value && moment(value).isValid()) {
//                    return moment(new Date(value)).format('MM/DD/YYYY');
//                }
//                return value;
//            })

//            //Disable Calendar
//            scope.$watch(attr.ngDisabled, function (newVal) {
//                if (newVal === true)
//                    $(elm).datepicker("disable");
//                else
//                    $(elm).datepicker("enable");
//            });

//            // Datepicker Settings
//            elm.datepicker({
//                autoSize: true,
//                changeYear: true,
//                changeMonth: true,
//                dateFormat: attr["dateformat"] || 'mm/dd/yy',
//                showOn: 'button',
//                buttonText: '<i class="glyphicon glyphicon-calendar"></i>',
//                onSelect: function (valu) {
//                    scope.$apply(function () {
//                        ctrl.$setViewValue(valu);
//                    });
//                    elm.focus();
//                },

//                beforeShow: function () {
//                    debugger;
//                    if (attr["minDate"] != null)
//                        $(elm).datepicker('option', 'minDate', attr["minDate"]);

//                    if (attr["maxDate"] != null)
//                        $(elm).datepicker('option', 'maxDate', attr["maxDate"]);
//                },


//            });
//        }
//    }
//});
//})();